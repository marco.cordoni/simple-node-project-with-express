// ------------- IMPORTS NODE MODULES -------------
const path = require("path")

// IMPORTS 3th PARTIES MODULES
const express = require("express")

// IMPORTS from my files
const adminRoutes = require("./routes/admin")
const shopRoutes = require("./routes/shop")

// ------------- INITIALIZE AN OBJECT OF EXPRESS FRAMEWORK -------------
const app = express()

// ------------- WE CAN ADD AN OTHERS MIDDLEWARE TO EXPRESS -------------
// a simple milddleware take 3 parameters:
//  - req: the request
//  - res: the response
//  - next: a function that has to be executed to allow the request to travel on the next middleware
// app.use((req, res, next) => {
// 	console.log("In the middleware!")
// 	next() // to allow travelling to the next custom middleware
// })

// app.use((req, res, next) => {
// 	console.log("In another middleware!")
// 	res.send("ciao") // send a body to the caller and implicitly call res.end()
// })

// to be able to use the body in a request, this middleware implicitly call next() at the end
app.use(express.json())

// we can also define a middleware to be triggered only for certain path of the url
// this middlware will be triggered only for path that start with  "/" (so all paths)
// and we use next() to continue to the next middleware who match this path
// APP.USE HANDLE ALL TYPES OF REQUEST (GET, POST, PUT, PATCH, DELETE)
app.use("/", (req, res, next) => {
	console.log("Always called")
	next()
})

// routes from others files, we can attach a path bedore all paths inside a route file
app.use("/admin", adminRoutes)
app.use(shopRoutes)

app.use("/", (req, res, next) => {
	res.status(404).sendFile(path.join(__dirname, "views", "404.html"))
})

// ------------- CREATE AND START THE SERVER -------------
// make it listening on the port 3000 (default is 80) and with the hostname "localhost" (default is already localhost)
// normally, these variables should be give from the environment
const port = 3000
const host = "localhost"
app.listen(port, host)

// console.log(`Alt+126 = ~`)
// console.log(`Alt+96 = \``)
