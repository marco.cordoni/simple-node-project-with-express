const path = require("path")
const express = require("express")

const router = express.Router()

router.get("/add-product", (req, res, next) => {
	console.log("Add a book GET")
	res.sendFile(path.join(__dirname, "..", "views", "add-product.html"))
})

// use these enpoints as POST, PUT and PATCH
// this middlware will be triggered only for POST requests equal to "/add-book"
// .USE() ISN'T AN EXACT MATCH THE OTHERS YES
router.post("/add-product", (req, res, next) => {
	console.log("Add a book POST")
	console.log(req.body)
	res.send(req.body)
})

router.put("/add-product", (req, res, next) => {
	console.log("Add a book PUT")
	res.send(req.body)
})

router.patch("/add-product", (req, res, next) => {
	console.log("Add a book PATCH")
	res.send(req.body)
})

module.exports = router
