const path = require("path")
const express = require("express")

const router = express.Router()

// this middlware will be triggered only for paths that start with  "/test"
// we don't use next() because in this middleware will be manage this request
router.use("/test", (req, res, next) => {
	console.log(`In the middleware for the path ${req.url} !`)
	res.send("Test")
})

router.use("/hello-world", (req, res, next) => {
	res.send("Hello World!")
})

// we can redirect a middleware to an other path handling
router.use("/redirect", (req, res, next) => {
	res.redirect("/hello-world")
})

router.get("/", (req, res, next) => {
	res.sendFile(path.join(__dirname, "..", "views", "shop.html"))
})

// the next middleware will be triggered when the previouses path (except for the ""Always called"")
// won't be match (so all paths because all paths start by "/")
// WE KEEP IT COMMENTED TO BE ABLE TO CATCH THE 404 CASE
// router.use("/", (req, res, next) => {
// 	console.log(`In the middleware for the path ${req.url} !`)
// 	res.send("Default case")
// })

module.exports = router
